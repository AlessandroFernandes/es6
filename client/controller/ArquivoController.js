class ArquivoController {

    constructor() {
        this._inputDados = document.querySelector('.dados-arquivo');

    }

    envia() {
        //cria um Arquivo com as suas propriedades;
        let arquivo = HelperCriacao.criar(this._inputDados.value);
        console.log(`Nosso arquivo ${arquivo.nome} ${arquivo.tamanho} ${arquivo.tipo}`)
         
        this._limpaFormulario();
    }

    _limpaFormulario() {
        this._inputDados.value = '';
        this._inputDados.focus();
    }

  
}