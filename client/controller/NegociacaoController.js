class NegociacaoController {

    constructor() {

        let $ = document.querySelector.bind(document);

        this._inData = $("#data");
        this._inQuantidade = $("#quantidade");
        this._inValor = $("#valor");
        this._inControle = new ControleNegociacao();

        this._tabela = new TabelaTemplate($("#template"));
        this._tabela.update(this._inControle);

        this._mensagem = new Mensagem();
        this._mensagemView = new MensagemView($("#MensagemView"));

    }

    submete(event) {

        event.preventDefault();
        this._inControle.adicionar(this._adicionarNegociacao());
        this._tabela.update(this._inControle);
        this._limparFormulario();

        this._mensagem.texto = 'Negocio cadastrado com sucesso';
        this._mensagemView.update(this._mensagem);
    }

    _limparFormulario() {

        this._inData.value = '';
        this._inQuantidade.value = 1;
        this._inValor.value = 0;

        return this._inData.focus();
    }

    _adicionarNegociacao() {

        return new Negociacao(
            HelperDate.textoParaData(this._inData.value),
            this._inQuantidade.value,
            this._inValor.value
        )
    }

}