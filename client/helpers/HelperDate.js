class HelperDate{

    constructor(){

        throw new Error("Classe nao pode ser instanciada");
    }

    static textoParaData(texto){
        return new Date(...texto.split("-").map((item, posicao) => item - posicao % 2));
    }
    
    static dataParaTexto(data){
        return `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
    }


}