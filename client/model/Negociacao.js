class Negociacao{

    constructor(data, quantidade, preco){
        this._data = new Date(data.getTime());
        this._quantidade = quantidade;
        this._preco = preco;
        Object.freeze(this);
    }

    get volume(){
        return this._quantidade * this._preco;
    }
    get data(){
        return new Date(this._data.getTime());
    }
    get quantidade(){
        return this._quantidade;
    }
    get preco(){
        return this._preco;
    }
}