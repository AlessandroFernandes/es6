class TabelaTemplate {

    constructor(template){

        this._template = template;

    }

    _tabela(model) {
                return `
                <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>DATA</th>
                        <th>QUANTIDADE</th>
                        <th>VALOR</th>
                        <th>VOLUME</th>
                    </tr>
                </thead>
                
                <tbody>
                   
                    ${model.mostrar.map(n => `

                          <tr>
                            <td>${HelperDate.dataParaTexto(n.data)}</td>
                            <td>${n.quantidade}</td>
                            <td>${n.preco}</td>
                            <td>${n.volume}</td>
                         </tr>`

                    ).join('')}
                 
                </tbody>
                
                <tfoot>
                    <td colspan="3"></td>
                    <td>${model.mostrar.reduce((total, n) => total += n.volume, 0.0)}</td>

                </tfoot>
                 </table>
        
                 `;
    }

    update(model){
        return this._template.innerHTML =  this._tabela(model);
    }
}