class MensagemView {

    constructor(elemento) {
        this._elemento = elemento;
    }


    _tabela(model) {
        return model.texto ?  `<p class="alert alert-info">${model.texto}</p>` : `<p></p>`;
    }

    update(model) {
        this._elemento.innerHTML = this._tabela(model);

    }


}
